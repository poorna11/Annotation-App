# Annotation-App #

# Summary

Annotation App is a firebase based project for audio editing purposes.The audio and corresponding text is fetched from firebase database
and user and edit the text and earn points by submitting it to database.

# Features 

1. It includes Google and Facebook sign in.
2. It verifies user through OTP verification that uses PhoneAuthcredentials.
3. It saves the personal details of the user in the database so if the user has uninstalled and then installed the app,it won't ask for the details again.
4. It fetches the audio and corresponding text from firebase database for the user to edit.

# Screenshots 

| Verification | Personal Details | Main Page |
| ------ | ------ | ------ |
| ![shot](/docs/ss1.png) | ![shot](/docs/ss2.png) | ![shot](/docs/ss3.png) | 
 



