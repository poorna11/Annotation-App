package english.android.com.guess_the_audio;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import english.android.com.guess_the_audio.utils.VollyHelper;

public class Details extends AppCompatActivity {
    VollyHelper volly;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phno)
    TextView phno;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.audio_submitted)
    TextView audio_submitted;
    @BindView(R.id.audio_accepted)
    TextView audio_accepted;
    @BindView(R.id.total_earning)
    TextView total_earning;
    @BindView(R.id.paid_amount)
    TextView paid_amount;
    @BindView(R.id.phEdit)
    Button phEdit;


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        toolbar.setTitle("Profile");
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setDisplayShowHomeEnabled(true);
//        }
        volly = new VollyHelper(this);
        name.setMovementMethod(new ScrollingMovementMethod());
        email.setMovementMethod(new ScrollingMovementMethod());
        fetchData();
    }

    public void fetchData() {
        try {
            volly.get("mydetails", null, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("text", response.toString());
                    displayData(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Log.e("text", "error");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayData(JSONObject response) {
        final String ph = response.optString("phoneNo", "");
        name.setText(response.optString("name", ""));
        email.setText(response.optString("emailId", ""));
        phno.setText(ph);
        audio_accepted.setText(String.format(Locale.US, "%.0f secs", response.optDouble("timeAccepted", 0)));
        audio_submitted.setText(String.format(Locale.US, "%.0f secs", response.optDouble("timeDecoded", 0)));
        total_earning.setText(String.format(Locale.US, "₹ %.2f", response.optDouble("totalEarnings", 0)));
        paid_amount.setText(String.format(Locale.US, "₹ %.2f", response.optDouble("amountPaid", 0)));
        phEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                (new EditPhone(Details.this, ph, volly)).show(Details.this.getFragmentManager(), "Edit Phone No");
            }
        });
    }
}
