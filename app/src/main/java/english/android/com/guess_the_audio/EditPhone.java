package english.android.com.guess_the_audio;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import english.android.com.guess_the_audio.utils.VollyHelper;

public class EditPhone extends DialogFragment {
    String ph;
    VollyHelper v;
    Details c;

    public EditPhone() {
        super();
    }

    public EditPhone(Details c, String ph, VollyHelper v) {
        super();
        this.c = c;
        this.ph = ph;
        this.v = v;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Enter your Phone Number");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fragment_edit_phone, null);

        builder.setView(dialogView);
        final EditText e = dialogView.findViewById(R.id.edit_ph_no);
        e.setText(ph);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                try {
                    String p = e.getText().toString();
                    if (p != null && p.length() == 10) {
                        JSONObject j = new JSONObject();
                        j.putOpt("phoneNo", p);
                        v.post("phoneNo", j, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                c.displayData(response);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(c, "Some error Occurred please try after some time.", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "Please Enter Correct Phone Number.", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(c, "Some error Occurred please try after some time.", Toast.LENGTH_LONG).show();
                }
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        return builder.create();
    }
}