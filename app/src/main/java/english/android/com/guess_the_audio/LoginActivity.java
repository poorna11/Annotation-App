package english.android.com.guess_the_audio;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import english.android.com.guess_the_audio.utils.VollyHelper;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static english.android.com.guess_the_audio.PronouncerNavigationDrawerActivity.REQUEST_PERMISSION_CODE;

public class LoginActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 9001;
    @BindView(R.id.btn_google)
    SignInButton mGoogleLoginButton;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    FirebaseAuth mFirebaseAuth;
    GoogleSignInClient mGoogleSignInClient;
    FirebaseUser mFirebaseUser;

    //CallbackManager mCallbackManager;
    private boolean isAppInstalled(String packageName) {
        try {
            PackageManager pm = getPackageManager();
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return pm.getApplicationInfo(packageName, 0).enabled;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
      //  Log.d("intent",""+isAppInstalled("com.google.android.apps.inputmethod.hindi"));
        //startActivity(new Intent("com.google.android.apps.inputmethod.hindi"));

        if (mFirebaseUser != null) {
            mFirebaseUser.getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                @Override
                public void onSuccess(GetTokenResult result) {
                    LoginActivity.this.updateUI(result.getToken());
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if (!checkPermission()) {
            requestPermission();
        }
        //Firebase auth instances
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        //Create Callback Manager
        //mCallbackManager = CallbackManager.Factory.create();

        configGoogleSignIn();

        mGoogleLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGoogle();
            }
        });

    }


    // [START signin]
    private void signInWithGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    // [START config_signin]
    // Configure Google Sign In
    private void configGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Pass the activity result back to the Facebook SDK
       // mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);


        //Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
                Toast.makeText(this, "Google SignIn failed!", Toast.LENGTH_SHORT).show();
                Log.e("EXCEPTION", " " + e);
            }
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        //make progress bar visible if auth is starts
        mProgressBar.setVisibility(View.VISIBLE);
        mGoogleLoginButton.setEnabled(false);


        AuthCredential authCredential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(authCredential)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            try {
                                FirebaseUser user = mFirebaseAuth.getCurrentUser();
                                if (user != null) {
                                    user.getIdToken(false).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
                                        @Override
                                        public void onSuccess(GetTokenResult result) {
                                            String idToken = result.getToken();
                                            //updateUI(idToken);
                                            //temp
                                            startActivity(new Intent(LoginActivity.this, OtpActivity.class));
                                            finish();
                                            Log.d("text", "GetTokenResult result = " + idToken);
                                        }
                                    });
                                } else {
                                    updateUI(null);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                updateUI(null);
                            }
                        } else {
                            updateUI(null);
                        }
                        mProgressBar.setVisibility(View.INVISIBLE);
                        mGoogleLoginButton.setEnabled(true);
                    }
                });


    }

    private void updateUI(final String token) {
        Log.d("test", token);

        if (token == null || token.isEmpty()) {
            Toast.makeText(this, "Authentication Failed! Please try again!", Toast.LENGTH_SHORT).show();
        } else {
            try {

                VollyHelper v = new VollyHelper(LoginActivity.this);
                JSONObject body = new JSONObject();
                body.put("token", token);
                v.post("login", body, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("text", response.toString());
                        try {
                            if (response.getBoolean("result")) {
                                VollyHelper.TOKEN = token;
                                startActivity(new Intent(LoginActivity.this, OtpActivity.class));
                                finish();
                            }
                        } catch (Exception e) {
                            Toast.makeText(LoginActivity.this, "1SignIn failed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(LoginActivity.this, "2SignIn failed!", Toast.LENGTH_SHORT).show();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(LoginActivity.this, "3SignIn failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (REQUEST_PERMISSION_CODE == requestCode && grantResults.length > 0) {
            boolean storagePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean recordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            boolean readStoragePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;

            if (storagePermission && recordPermission && readStoragePermission) {
                Toast toast = Toast.makeText(LoginActivity.this, "Permission Granted", Toast.LENGTH_SHORT);
                toast.show();

            } else {

                Toast toast = Toast.makeText(LoginActivity.this, "Permission Denied. Buttons Disabled!", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED;
    }

}
