package english.android.com.guess_the_audio;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import butterknife.BindView;

public class OtpActivity extends AppCompatActivity {

    EditText editTextPhone;
    MaterialButton buttonGetVerificationCod;
    MaterialCheckBox check_box_;
    FirebaseAuth mAuth;
    boolean checked;
    static String phone, codeSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        mAuth = FirebaseAuth.getInstance();

        checked = false;
        editTextPhone = findViewById(R.id.editTextPhone);
        buttonGetVerificationCod = findViewById(R.id.buttonGetVerificationCode);
        check_box_ = findViewById(R.id.check_box_);

        check_box_.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    checked = true;
                } else {
                    checked = false;
                }

            }
        });

        buttonGetVerificationCod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validPhoneNo(editTextPhone.getText().toString()) && checked) {

                    Log.e("VALID", " ");
                    startActivity(new Intent(OtpActivity.this, PronouncerNavigationDrawerActivity.class));
                    finish();

                } else {

                    if (!validPhoneNo(editTextPhone.getText().toString()) && checked) {
                        new MaterialAlertDialogBuilder(OtpActivity.this, R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered)
                                .setMessage("Please enter valid phone number!")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                    }
                                })
                                //.setNegativeButton("Cancel", null)
                                .show();
                    }

                    if (!checked && validPhoneNo(editTextPhone.getText().toString())) {
                        new MaterialAlertDialogBuilder(OtpActivity.this, R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered)
                                .setMessage("Please check the privacy policy!")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                    }
                                })
                                //.setNegativeButton("Cancel", null)
                                .show();
                    }

                    if (!checked && !validPhoneNo(editTextPhone.getText().toString())) {
                        new MaterialAlertDialogBuilder(OtpActivity.this, R.style.ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered)
                                .setMessage("Please enter valid phone number and check the privacy policy!")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                    }
                                })
                                //.setNegativeButton("Cancel", null)
                                .show();
                    }
                    // Log.e("INVALID", " ");
                }

            }
        });

//        editTextPhone = findViewById(R.id.editTextPhone);
//
//        findViewById(R.id.buttonGetVerificationCode).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendVerificationCode();
//            }
//        });


//        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                verifySignInCode();
//            }
//        });
    }

    private boolean validPhoneNo(String phone) {

        if (phone != null && phone.length() == 10 && !phone.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

//    private void verifySignInCode(){
//        String code = editTextCode.getText().toString();
//        try{
//        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(codeSent, code);
//        signInWithPhoneAuthCredential(credential);
//       }  catch (Exception e){
//        Toast toast = Toast.makeText(this, "Verification Code is wrong", Toast.LENGTH_SHORT);
//        // toast.setGravity(Gravity.CENTER,0,0);
//        toast.show();
//    }
//    }
//
//    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
//        mAuth.signInWithCredential(credential)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            //here you can open new activity
//                            Toast.makeText(getApplicationContext(),
//                                    "Login Successfull", Toast.LENGTH_LONG).show();
//                        } else {
//                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
//                                Toast.makeText(getApplicationContext(),
//                                        "Incorrect Verification Code ", Toast.LENGTH_LONG).show();
//                            }
//                        }
//                    }
//                });
//    }
//
//    private void sendVerificationCode(){
//
//         phone = editTextPhone.getText().toString();
//
//        if(phone.isEmpty()){
//            editTextPhone.setError("Phone number is required");
//            editTextPhone.requestFocus();
//            return;
//        }
//
//        if(phone.length() < 10 ){
//            editTextPhone.setError("Please enter a valid phone");
//            editTextPhone.requestFocus();
//            return;
//        }
//
//        System.out.println(phone);
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(
//                "+91"+phone,        // Phone number to verify
//                60,                 // Timeout duration
//                TimeUnit.SECONDS,   // Unit of timeout
//                OtpActivity.this,               // Activity (for callback binding)
//                mCallbacks);        // OnVerificationStateChangedCallbacks
//    }
//
//
//
//    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
//
//        @Override
//        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
//            Toast.makeText(OtpActivity.this,"Verified",Toast.LENGTH_SHORT).show();
//            Intent in=new Intent(OtpActivity.this,LoginDetailsActivity.class);
//            startActivity(in);
//            finish();
//        }
//
//        @Override
//        public void onVerificationFailed(FirebaseException e) {
//            // Toast.makeText(OtpActivity.this,"verification fialed",Toast.LENGTH_SHORT).show();
//            if (e instanceof FirebaseAuthInvalidCredentialsException) {
//                Toast.makeText(OtpActivity.this,"invalid req",Toast.LENGTH_SHORT).show();
//                // Invalid request
//                // ...
//            } else if (e instanceof FirebaseTooManyRequestsException) {
//                Toast.makeText(OtpActivity.this,"sms quota exceeded",Toast.LENGTH_SHORT).show();
//                // The SMS quota for the project has been exceeded
//                // ...
//            }
//        }
//
//        @Override
//        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
//            super.onCodeSent(s, forceResendingToken);
//            System.out.println("code sent!!!!!");
//            codeSent = s;
//            Toast.makeText(OtpActivity.this,"Code sent",Toast.LENGTH_SHORT).show();
//            Intent in=new Intent(OtpActivity.this,VerifyOtpActivity.class);
//            startActivity(in);
//            finish();
//        }
//    };


}