package english.android.com.guess_the_audio;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.chibde.visualizer.LineBarVisualizer;
import com.google.android.material.button.MaterialButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import english.android.com.guess_the_audio.adapters.RecyclerAdapter;
import english.android.com.guess_the_audio.utils.MyAudio;
import english.android.com.guess_the_audio.utils.VollyHelper;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class PronouncerNavigationDrawerActivity extends AppCompatActivity {

    public static final int REQUEST_PERMISSION_CODE = 1;
    //ButterKnife View Injection
    @BindView(R.id.textView2)
    TextView tv2;
    @BindView(R.id.tv_translated_text)
    TextView translatedText;
    @BindView(R.id.player)
    ImageView mPlayButton;
    @BindView(R.id.audio_indicator)
    LineBarVisualizer mLineBarVisualizer;
    @BindView(R.id.audio_seek_bar)
    SeekBar mSeekBar;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.sendButton)
    MaterialButton mSendButton;
    // MyAudio myAudio;
    private MediaPlayer mMediaPlayer;
    private Runnable mRunnable;
    private Handler mHandler;
    private VollyHelper volly;
    MyAudio myAudio;
    HashMap<String, String> dic;
    ArrayList<String> feedsArrayList;
    RecyclerAdapter recyclerAdapter;
    LinearLayoutManager layoutManager;

    public void translitrate() {
        final String temp = "Please type in hindi language. \n कृपया हिंदी भाषा में टाइप करें|";
        translatedText.setText(temp);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        startActivity(new Intent("android.settings.INPUT_METHOD_SETTINGS"));
//
//        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
//        imeManager.showInputMethodPicker();
        //  mPlayButton.setEnabled(false);
        mHandler = new Handler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.prononcer_navigation_drawer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_details) {
            startActivity(new Intent(PronouncerNavigationDrawerActivity.this, Details.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.content_pronouncer_navigation_drawer);
            ButterKnife.bind(this);

            if (!checkPermission()) {
                requestPermission();
            }
            volly = new VollyHelper(this);
            translitrate();
            mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser && mMediaPlayer != null) {
                        mMediaPlayer.seekTo(progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });


            feedsArrayList = new ArrayList<>();


            feedsArrayList.add("Facebook Feed 1");
            feedsArrayList.add("Facebook Feed 2");
            feedsArrayList.add("Facebook Feed 3");
            feedsArrayList.add("Facebook Feed 4");
            feedsArrayList.add("Facebook Feed 5");

            recyclerView.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(PronouncerNavigationDrawerActivity.this, RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerAdapter = new RecyclerAdapter(PronouncerNavigationDrawerActivity.this, feedsArrayList);
            recyclerView.setAdapter(recyclerAdapter);

            mSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
//                        final String audioText = mEnterEditText.getText().toString();
//                        if (audioText.length() == 0 || myAudio == null) {
//                            mEnterEditText.setError("Enter the correct text");
//
//                        } else if (!audioText.matches("([\\u0900-\\u097F]| )*")) {
//                            mEnterEditText.setError("Please enter only hindi characters.");
//                        } else {
//                            JSONObject userData = new JSONObject();
//                            userData.put("_id", myAudio._id);
//                            userData.put("text", audioText);
//                            Log.d("text", userData.toString());
//
//                            volly.post("usersubmit", userData, new Response.Listener<JSONObject>() {
//                                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//                                @Override
//                                public void onResponse(JSONObject response) {
//                                    MyAudio m = new MyAudio(PronouncerNavigationDrawerActivity.this, response);
//                                    if (m.result) {
//                                        mEnterEditText.setText("");
//                                        myAudio = m;
//                                        handleAudio();
//                                    }
//                                }
//                            }, new Response.ErrorListener() {
//                                @Override
//                                public void onErrorResponse(VolleyError error) {
//                                    error.printStackTrace();
//                                    new MyAudio(PronouncerNavigationDrawerActivity.this, null);
//                                    Log.e("text", "error");
//                                }
//                            });
//
//                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }
                }
            });
            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if (mMediaPlayer != null) {
                        if (mMediaPlayer.isPlaying()) {
                            mPlayButton.setBackground(getApplicationContext().getDrawable(R.drawable.play1));
                            mMediaPlayer.pause();
                        } else {

                            mPlayButton.setBackground(getApplicationContext().getDrawable(R.drawable.pause1));
                            mMediaPlayer.start();
                            playProgress();

                        }
                    } else {
                        Toast.makeText(PronouncerNavigationDrawerActivity.this, "Please wait for Media to load", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            volly.get("audio", null, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onResponse(JSONObject response) {
                    MyAudio m = new MyAudio(PronouncerNavigationDrawerActivity.this, response);
                    if (m.result) {
                        myAudio = m;
                        handleAudio();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    new MyAudio(PronouncerNavigationDrawerActivity.this, null);
                    Log.e("text", "error");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void handleAudio() {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
            mMediaPlayer = new MediaPlayer();
            mLineBarVisualizer.setEnabled(false);
            mLineBarVisualizer.setPlayer(mMediaPlayer.getAudioSessionId());
            mLineBarVisualizer.setDensity(70);
            if (!checkPermission()) {
                mPlayButton.setEnabled(false);
                mLineBarVisualizer.setEnabled(false);
                return;
            }
            mPlayButton.setBackground(PronouncerNavigationDrawerActivity.this.getApplicationContext().getDrawable(R.drawable.play1));
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {


                    mPlayButton.setEnabled(true);
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mSeekBar.setMax(mediaPlayer.getDuration());
                }
            });
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mPlayButton.setBackground(getApplicationContext().getDrawable(R.drawable.play1));
                    mMediaPlayer.reset();
                }
            });
            mProgressBar.setVisibility(View.VISIBLE);
            tv2.setText(myAudio.FileName);
            mMediaPlayer.setDataSource(myAudio.FileLink);
            mMediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void playProgress() {
        if (mMediaPlayer != null) {
            mSeekBar.setProgress(mMediaPlayer.getCurrentPosition());
            if (mMediaPlayer.isPlaying()) {
                mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        playProgress();
                    }
                };
                mHandler.postDelayed(mRunnable, 100);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
        mHandler.removeCallbacks(mRunnable);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(PronouncerNavigationDrawerActivity.this, new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO, READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (REQUEST_PERMISSION_CODE == requestCode && grantResults.length > 0) {
            boolean storagePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            boolean recordPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            boolean readStoragePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;

            if (storagePermission && recordPermission && readStoragePermission) {
                Toast toast = Toast.makeText(PronouncerNavigationDrawerActivity.this, "Permission Granted", Toast.LENGTH_SHORT);
                toast.show();

            } else {

                Toast toast = Toast.makeText(PronouncerNavigationDrawerActivity.this, "Permission Denied. Buttons Disabled!", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), RECORD_AUDIO);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED;
    }
}
