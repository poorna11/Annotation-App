package english.android.com.guess_the_audio.adapters;

import android.content.Context;
import androidx.annotation.NonNull;

import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import butterknife.BindView;
import english.android.com.guess_the_audio.R;
import english.android.com.guess_the_audio.utils.MyAudio;
import english.android.com.guess_the_audio.utils.VollyHelper;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<String> arrayList;
    MyAudio myAudio;
    VollyHelper volly;

    public RecyclerAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lay_recycler_adapter, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.MyViewHolder holder, int position) {

        holder.mTitle.setText(String.valueOf(arrayList.get(position)));
//                                final String audioText = mEnterEditText.getText().toString();
//                        if (audioText.length() == 0 || myAudio == null) {
//                            mEnterEditText.setError("Enter the correct text");
//
//                        } else if (!audioText.matches("([\\u0900-\\u097F]| )*")) {
//                            mEnterEditText.setError("Please enter only hindi characters.");
//                        } else {
//                            JSONObject userData = new JSONObject();
//                            userData.put("_id", myAudio._id);
//                            userData.put("text", audioText);
//                            Log.d("text", userData.toString());
//
//                            volly.post("usersubmit", userData, new Response.Listener<JSONObject>() {
//                                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//                                @Override
//                                public void onResponse(JSONObject response) {
//                                    MyAudio m = new MyAudio(context, response);
//                                    if (m.result) {
//                                        mEnterEditText.setText("");
//                                        myAudio = m;
//                                        handleAudio();
//                                    }
//                                }
//                            }, new Response.ErrorListener() {
//                                @Override
//                                public void onErrorResponse(VolleyError error) {
//                                    error.printStackTrace();
//                                    new MyAudio(context, null);
//                                    Log.e("text", "error");
//                                }
//                            });
//
//                        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView mTitle;
        EditText editText;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTitle=itemView.findViewById(R.id.textViewFeed);
            editText=itemView.findViewById(R.id.editText);
        }
    }
}
