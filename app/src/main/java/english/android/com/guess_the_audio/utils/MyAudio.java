package english.android.com.guess_the_audio.utils;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONObject;

public class MyAudio {

        public String FileName;
        public String FileLink;
        public String _id;
        public int rate;
        public boolean result;
        public String error;

        public MyAudio(Context c, JSONObject res) {
            String errorMsg = "Server Error. Please Try again After some time.";
            if (res == null) {
                Toast.makeText(c, errorMsg, Toast.LENGTH_LONG).show();
            } else {
                try {
                    this.result = res.optBoolean("result", false);
                    if (this.result) {
                        this.FileLink = res.getString("FileLink");
                        this.FileName = res.getString("FileName");
                        this._id = res.getString("_id");
                        this.rate = res.getInt("rate");
                    } else {
                        throw new Exception();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    this.error = res.optString("error", errorMsg);
                    Toast.makeText(c, this.error, Toast.LENGTH_LONG).show();
                }
            }


        }
    }


