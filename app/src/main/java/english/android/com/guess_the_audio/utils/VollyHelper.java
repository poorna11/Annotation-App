package english.android.com.guess_the_audio.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import english.android.com.guess_the_audio.R;

public  class VollyHelper {

    private final Context mContext;
    private final RequestQueue mRequestQueue;
    public static String TOKEN = null;

    public static String mBaseUrl;

    public VollyHelper(Context c) {
        mContext = c;
        mBaseUrl = c.getString(R.string.baseUri);
        mRequestQueue = Volley.newRequestQueue(mContext);
        Log.d("text", mBaseUrl);

    }

    private String contructUrl(String method) {
        return mBaseUrl + "/" + method;
    }

    /* public ImageLoader getImageLoader(){
        return mImageLoader;
    }*/

    public void get(String method, JSONObject jsonRequest,
                    Listener<JSONObject> listener, ErrorListener errorListener) {

        JsonObjectRequest objRequest = new JsonObjectRequest(Method.GET, contructUrl(method), jsonRequest, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", TOKEN);
                return params;
            }
        };
        objRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(objRequest);
    }

    public void getArr(String method, JSONArray jsonRequest,
                       Listener<JSONArray> listener, ErrorListener errorListener) {

        JsonArrayRequest objRequest = new JsonArrayRequest(Method.GET, contructUrl(method), jsonRequest, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", TOKEN);
                return params;
            }
        };
        objRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(objRequest);
    }


    public void post(String method, JSONObject jsonRequest,
                     Listener<JSONObject> listener, ErrorListener errorListener) {

        JsonObjectRequest objRequest = new JsonObjectRequest(Method.POST, contructUrl(method), jsonRequest, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", TOKEN);
                return params;
            }
        };
        objRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(objRequest);
    }

    public void postArr(String method, JSONArray jsonRequest,
                        Listener<JSONArray> listener, ErrorListener errorListener) {

        JsonArrayRequest objRequest = new JsonArrayRequest(Method.POST, contructUrl(method), jsonRequest, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", TOKEN);
                return params;
            }
        };
        objRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(objRequest);
    }


}